<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {return [CONTROLLER_STATUS_OK];}

if ($mode == 'manage') {
	
    $selected_fields = Tygh::$app['view']->getTemplateVars('selected_fields');

    $selected_fields[] = array(
        'name' => '[products_data][prod_condit]',
        'text' => __('my_conditions')
    );
   Tygh::$app['view']->assign('selected_fields', $selected_fields);
   
} 
elseif 
($mode == 'm_update') 
{
    $selected_fields = Tygh::$app['session']['selected_fields'];

    if (!empty($selected_fields['products_data']['prod_condit'])) {
    	
    	$field_groups = Tygh::$app['view']->getTemplateVars('field_groups');
        $filled_groups = Tygh::$app['view']->getTemplateVars('filled_groups');
    	
        $field_groups['S']['prod_condit']['name'] = 'products_data';
        $field_groups['S']['prod_condit']['variants'] = array(
                    'destroyed' => 'destroyed',
                    'poor' => 'poor',
                    'good' => 'good',
                    'average' => 'average',
                    'excellent' => 'excellent',
        );
        $filled_groups['S']['prod_condit'] = __('my_conditions');
	   
    Tygh::$app['view']->assign('field_groups', $field_groups);
    Tygh::$app['view']->assign('filled_groups', $filled_groups);
    }
}
