<?php

use Tygh\Enum\Addons\Conditions\ConditionsTypes;

if (!defined('BOOTSTRAP')) {die('Access denied'); }
 
 function fn_my_conditions_get_products($params, &$fields, $sortings, $condition, $join, $sorting, $group_by, $lang_code, $having)
 {
	 $fields['prod_condit'] = 'products.prod_condit';
 }
 
 function fn_my_conditions_get_conditions_types()
 {
	 return ConditionsTypes::getAll();
 }
 
 