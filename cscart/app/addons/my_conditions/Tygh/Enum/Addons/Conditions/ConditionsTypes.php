<?php
namespace Tygh\Enum\Addons\Conditions;

class ConditionsTypes
{
   public static function getAll()
    {
		return array('destroyed', 'poor', 'good', 'average', 'excellent');
    }
}